# FacialRecog

FacialRecog is a web based attendance application that allows student to login to via facial recognition system.

Group Name:  Skywalkers

Group Member Name  		   			Email Address
1.		Aayush Sapkota					96.estd@gmail.com
2.		Aasish Adhikari 				gemini.aasis@gmail.com
3.		Saugat Pathak 				pathaksaugat@hotmail.co.uk
4.		Mutiu Adelekan 				matyk2012@yahoo.com

Client: Dr Bharanidharan Shanmugam(MCA, PhD)
Email: bharanidharan.shanmugam@cdu.edu.au

Bitbucket Repo url: https://bitbucket.org/Aayush96/facialrecog

Repo instructions:
This repo can be accessed via URL https://bitbucket.org/Aayush96/facialrecog and this is also a private repository so you will need permission to view it.
This repo has 2 main branches master where all finalized production changes are kept and develop where development changes are pushed, we will also use git-flow to manage each feature of the app.

Aayush is the admin of the repo so, if you want to get access to the repo or be one of the contributors please, email him at his contact.

RUN "npm install" before using application otherwise your program WILL NOT WORK
