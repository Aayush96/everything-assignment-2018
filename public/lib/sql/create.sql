USE eaDB;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  id int(11) NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  userType varchar(12) NOT NULL CHECK(userType IN ('admin','normal')),
  # Assuming SHA256 hash
  hashed_password char(64) NOT NULL,
  # Assuming 16 chars in salt
  salt char(16) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `uploads`;

CREATE TABLE `uploads` (
  id int(11) NOT NULL auto_increment,
  userId int(11) NOT NULL,
  uploadsName varchar(255) NOT NULL,
  PRIMARY KEY (id),
  filePath varchar(1024) NOT NULL,
  year date NOT NULL,
  sem varchar(6) NOT NULL,
  CONSTRAINT FK_userId FOREIGN KEY (userId) references users(id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
