<div class="app wrap">
  <div class="container">
    <h1>Hi, <?php echo $name; ?></h1>
  </div>
</div>

<section class="app__files">
  <div class="file-filter">
    <div class="container">
      <form class="file-filter__form" action="/dashboard/filter/" method="get">
        <input type="text" name="search" value="" placeholder="Search..." title="search">
        <select class="drop_down__filter" name="year" title="year">
          <option value="">Choose an year</option>
          <?php
          //date options to show
          if($date != null){
            $dateArr = [];
            for($i = 0; $i < count($date); $i++){
              // echo $date[$i]['year'];
              $dateVar = $date[$i]['year'];
              $arrDateVar = explode("-",$dateVar);
              $year = $arrDateVar[0];
              if (!in_array($year, $dateArr)){
                echo "<option value=\"{$year}\">$year</option>";
              }
              array_push($dateArr,$year);
            }
          } else {
            echo "<option value=\"\">No assignment present</option>";
          }
          ?>
        </select>
        <select class="drop_down__filter" name="sem" title="sem">
          <option value="">Choose an semester</option>
          <option value="Semester 1">Semester 1</option>
          <option value="Semester 2">Semester 2</option>
          <option value="Semester 3">summer semester</option>
        </select>
        <input type="submit" value="Filter">
        <input type="reset" value="Reset filters">
      </form>
    </div>
  </div>

  <div class="summary-card__outer-wrap">
    <div class="container">
      <div class="summary-card__grid">
        <?php if(!empty($uploads)){
          for($i=0;$i<count($uploads);$i++){
            $upload = $uploads[$i];
            echo "<div class=\"summary-card col-md-6 col-lg-4 \">";
            foreach ($upload as $key => $value) {
              $date = $upload['year'];
              $sem = $upload['sem'];
              $id = $upload['id'];
              if($key == "uploadsName"){
                echo "<div class='summary-card__text'>
                <p>{$date} | {$sem}</p>
                <h3 class='summary-card__title summary-card__title-wrap'>{$value}</h3>
                </div>";
              }
              if($key == "filePath"){
                $value = str_replace($_SERVER['DOCUMENT_ROOT'],"",$value);
                echo "<div class='summary-card__btn-wrap'>
                <a target='about_blank' href=\"{$value}\" class='summary-card__btn btn'>View</a>
                <a target='about_blank' href=\"/item/{$id}\" class='summary-card__btn btn'>Edit</a>
                </div>
                <form action=\"/uploads/delete\" method=\"post\">
                <input type='hidden' value='post' name='_method' />
                <input type='hidden' name='id' value='{$id}'/>
                <input type='submit' class='btn' value='delete' />
                </form>
                ";
              }
            }
            echo "</div>";
          }
        } else {
          echo "<div class='summary-card'>";
          echo "<p>
          No results found.
          </p>
          </div>";
        } ?>
      </div>
    </div>
  </div>

  <div class="app__files-upload wrap">
    <div class="container">
      <h2>Add Assignment files</h2>
      <form class="app__upload-form" enctype="multipart/form-data" action="/upload" method="post" >
        <input type="hidden" name="_method" value="POST">
        <div class="form-control">
          <label for="upload-file" class="form__label">Select file to upload</label>
          <input type="file" name="upload-file" required class="form__input-file">
        </div>
        <div class="form-control">
          <label for="upload-file" class="form__label-inline">Assignment year: </label>
          <input type="date" name="year" value="" placeholder="2015" class="form__input-date" required>
        </div>

        <div class="form-control">
          <label for="sem" class="form__label-inline">Semester: </label>
          <select class="drop_down__filter" name="sem" required>
            <option value="sem1">Semester 1 (Feb - july )</option>
            <option value="sem2">Semester 2 (August - November)</option>
            <option value="sem3">Summer semester</option>
          </select>
        </div>
        <input type="submit" name="submit" value="&#8683; Upload" class="btn">
      </form>
    </div>
  </div>
</section>
