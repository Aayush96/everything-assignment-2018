<section class="home-banner">
  <div class="container">
    <div class="home-banner__text">
      <h1 class="home-banner__title">Everything Assignment</h1>
      <p>A portal to store your assignment and be able to sort them based on their categories and search them by date and semester is something commercial applications will not provide.</p>
      <p>Every Assignment is the solution to the need of students.</p>
    </div>
    <div class="home-banner__form">
      <div class="hommer-banner__form-wrap">
        <h2 class="home-banner__form-title">Start making better choices!</h2>
        <div class="home-banner__btn-wrap btn__wrap">
          <?php if(isset($authenticated) && $authenticated == true){?>
            <h3>Already Signed in!</h3>
          <?php } else { ?>
            <button class="home-banner__btn btn modal__btn" id='sign_lecturer'>Sign in</button>
          <?php } ?>
        </div>
      </div>
      </div>
  </div>
</section>
<!-- hidden login forms start -->
<div class="modal modal--lec" aria-label="hidden">
  <div class="modal__wrap">
    <h3 class="modal__title">Sign in</h3>
    <button type="button" name="button" class="modal__btn--close modal__close-btn">X</button>

    <form class="modal__form" action="" method="post">
      <label for="username">Username</label>
      <input type="text" id="username" name="username" value="" placeholder="Username" required />
      <label for="password">Password</label>
      <input type="password" id="password" name="password" value="" placeholder="Password" required class="model__input--last">
      <div class="modal__btn-wrap">
        <button type="submit" class="btn" name="button">Sign in</button>
      </div>
    </form>

    <a href="/signup" class="modal__forgot-cta">Dont have an account yet?</a>
  </div>
</div>
