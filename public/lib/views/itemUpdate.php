<div class="app__files-upload wrap">
  <div class="container">
    <h2>Update Assignment Details</h2>
    <form class="app__upload-form" action="/itemUpdate/" method="post" >
      <input type="hidden" name="_method" value="POST">
      <div class="form-control">
        <label for="name" class="form__label-inline">File Name</label>
        <input type="hidden" value="<?php echo $itemId;?>" name="id">
        <input type="text" name="name" value="<?php
        if($result != null){
          echo $result['uploadsName'];
        }
        ?>" placeholder="" class="form__input" required>
      </div>
      <div class="form-control">
        <label for="upload-file" class="form__label-inline">Due date(past date / future): </label>
        <input type="date" name="year" value="<?php
        if($result != null){
          echo $result['year'];
        }
        ?>" placeholder="2015" class="form__input-date" required>
      </div>

      <div class="form-control">
        <label for="sem" class="form__label-inline">Semester: </label>
        <select class="drop_down__filter" name="sem" required value="<?php
        if($result != null){
          echo $result['sem'];
        }
        ?>">
        <option value="sem1">Semester 1 (Feb - july )</option>
        <option value="sem2">Semester 2 (August - November)</option>
        <option value="sem3">Summer semester</option>
      </select>
    </div>
    <input type="submit" name="submit" value="Update" class="btn">
  </form>
</div>
</div>
</section>
