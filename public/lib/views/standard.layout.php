
<?php
$user = new User();
$is_authenticated = false;
if($user->is_authenticated()){
  $is_authenticated = true;
}
$isAdmin = false;
if($user->is_admin()){
  $isAdmin = true;
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title><?php echo $title; ?> | Everything Assignment</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo CSSPATH;?>" />
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
</head>
<body>

  <div id="mobile-bar">
    <a href="/">
      <h3 class="mobile-nav__title">Everything Assignment</h3>
    </a>
  </div>
  <input type="checkbox" class="check" id="checked">
  <label class="menu-btn" for="checked">
    <span class="bar top"></span>
    <span class="bar middle"></span>
    <span class="bar bottom"></span>
    <span class="menu-btn__text"></span>
  </label>
  <label class="close-menu" for="checked"></label>
  <nav class="drawer-menu">
    <ul>
      <?php
      if($is_authenticated == false){
        ?>
        <li><a href="/">Home</a></li>
        <li>
          <a href="/signup">Signup</a>
        </li>
      <?php }
      if($isAdmin == true) {?>
        <li>
          <a href="/members">Members</a>
        </li>
      <?php }
      if($is_authenticated == true){
        ?>
        <li>
          <a href="/dashboard">Dashboard</a>
        </li>
        <li>
          <a href="/account">Account</a>
        </li>
        <li>
          <a href="/signout" class='btn'>Log out</a>
        </li>
      <?php }?>


    </ul>
  </nav>

  <div id="desk-nav">

    <a href="/">
      <img class="desk-nav__logo" src="/uploads/app/logo.png" alt="Logo">
    </a>
    <nav class="desk-nav">
      <ul>
        <?php
        if($is_authenticated == false){
          ?>
          <li><a href="/">Home</a></li>
          <li>
            <a href="/signup">Signup</a>
          </li>
        <?php }
        if($isAdmin == true) {?>
          <li>
            <a href="/members">Members</a>
          </li>
        <?php }
        if($is_authenticated == true){
          ?>
          <li>
            <a href="/dashboard">Dashboard</a>
          </li>
          <li>
            <a href="/account">Account</a>
          </li>
          <li>
            <a href="/signout" class='btn'>Log out</a>
          </li>
        <?php }?>


      </ul>
    </nav>
  </div>


  <?php
  if(!empty($flash)){
    echo "<div class='form__error-wrap'>
    <p class='form__flash'>{$flash}</p>
    </div>";
  }
  if(!empty($error)){
    echo "<div class='form__error-wrap'>
    <p class='form__flash'>{$error}</p>
    </div>";
  }?>
  <?php
  require $content;
  ?>


</main>
<footer class="footer">
  <div class="container">
    <div class="footer__inner-wrap">
      <div class="footer__item col-sm-6 col-md-4">
        <h3 class="footer__item-title">Site map</h3>
        <nav class="footer__links rte">
          <ul class="footer__links-list">
            <li><a href="/">Home</a></li>
            <li><a href="/contact">Contact</a></li>
            <?php
            if($is_authenticated == true){
              ?>
              <li><a href="/dashboard">Dashboard</a></li>
              <li><a href="/account">Account</a></li>
            <?php }?>
          </ul>
        </nav>

        <p class="footer-company-name">Everything Assignment &copy; 2018</p>
      </div>

      <div class="footer__item col-sm-6 col-md-4">
        <h3 class="footer__item-title">About Developer</h3>
        <p>&#9786; Aayush Sapkota</p>
        <p><a href="mailto:96.estd@gmail.com">&#x2709; 96.estd@gmail.com</a></p>
      </div>

      <div class="footer__item col-sm-6 col-md-4">
        <h3 class="footer__item-title">Everything Assignment</h3>
        <p class="footer__company-about">
          We store any kind of file for you. Our search filter is one of features that this application provides. Also, the great design will help you with your assignments.
        </p>
      </div>
    </div>
  </div>
</footer>
<script type="text/javascript" src="<?php echo JSPATH;?>"></script>
</body>
</html>
