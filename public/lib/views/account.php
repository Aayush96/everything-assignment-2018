<section class="app__wrap">
  <div class="container">
    <h1>Your Details</h1>
    <div>
      <form action='/account/update' method='POST'>
        <input type='hidden' name='_method' value='post' />
        <div class="form-control">
          <label for='name' class="form__label">Username</label>
          <input type='text' id='name' name='name' autofocus required placeholder="Batman" class="form__input form__input--in-bg" value='<?php echo $result['name'];?>'/>
        </div>
        <div class="form-control">
          <?php
            echo "<p>User Type: {$result['userType']}</p>";
          ?>
        </div>
        <input type='submit' value='Update' class="btn"/>
      </form>
    </div>
  </div>
</section>
