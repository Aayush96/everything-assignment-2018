<section class="app__files">

  <div class="file-filter">
    <div class="container">
      <form class="file-filter__form" action="/dashboard/filter/" method="get">
        <input type="text" name="search" value="" placeholder="Search..." title="search">
        <select class="drop_down__filter" name="year" title="year">
          <option value="">Choose an year</option>
          <?php
          //get date options to show
          if($date != null){
            $dateArr = [];
            for($i = 0; $i < count($date); $i++){
              // echo $date[$i]['year'];
              $dateVar = $date[$i]['year'];
              $arrDateVar = explode("-",$dateVar);
              $yearInNow = $arrDateVar[0];
              if (!in_array($yearInNow, $dateArr)){
                echo "<option value=\"{$yearInNow}\">$yearInNow</option>";
              }
              array_push($dateArr,$yearInNow);
            }
          } else {
            echo "<option value=\"\">No assignment present</option>";
          }
          ?>
        </select>
        <select class="drop_down__filter" name="sem" title="sem">
          <option value="">Choose an semester</option>
          <option value="sem1">Semester 1</option>
          <option value="sem2">Semester 2</option>
          <option value="sem3">summer semester</option>
        </select>
        <input type="submit" value="Filter">
        <input type="reset" value="Reset filters">
      </form>
    </div>
  </div>

  <div class="summary-card__outer-wrap">
    <div class="container">
      <h1 class="summary-card__grid-title">

        <?php
        if($search != "" && $year != "" && $sem != "" ){
          echo "Search - \"{$search}\", year -  \"{$year}\" and sem - \"{$sem}\"results";
        }
        elseif($search != "" && $year != ""){
          echo "Search - \"{$search}\" and year -  \"{$year}\" results";
        }
        elseif($sem != "" && $year != ""){
          echo "Sem - \"{$sem}\" and year -  \"{$year}\" results";
        }
        elseif($search != "" && $sem != ""){
          echo "Search - \"{$search}\" and sem -  \"{$sem}\" results";
        }
        elseif($search != ""){
          echo "Results for \"{$search}\" ";
        }
        elseif($year != ""){
          echo "Results for year - \"{$year}\" ";
        }
        elseif($sem != ""){
          echo "Results for sem - \"{$sem}\" ";
        }
        else {
          echo "Results for \"\" ";
        }

        ?>
      </h1>

      <div class="summary-card__grid">
        <?php if(!empty($uploads)){
          for($i=0;$i<count($uploads);$i++){
            $upload = $uploads[$i];
            echo "<div class=\"summary-card col-md-6 col-lg-4 \">";
            foreach ($upload as $key => $value) {
              $date = $upload['year'];
              $sem = $upload['sem'];
              $id = $upload['id'];
              if($key == "uploadsName"){
                echo "<div class='summary-card__text'>
                <p>{$date} | {$sem}</p>
                <h3 class='summary-card__title summary-card__title-wrap'>{$value}</h3>
                </div>";
              }
              if($key == "filePath"){
                $value = str_replace($_SERVER['DOCUMENT_ROOT'],"",$value);
                echo "<div class='summary-card__btn-wrap'>
                <a target='about_blank' href=\"{$value}\" class='summary-card__btn btn'>View</a>
                <a target='about_blank' href=\"/item/{$id}\" class='summary-card__btn btn'>Edit</a>
                </div>
                <form action=\"/uploads/delete\" method=\"post\">
                <input type='hidden' value='post' name='_method' />
                <input type='hidden' name='id' value='{$id}'/>
                <input type='submit' class='btn' value='delete' />
                </form>
                ";
              }
            }
            echo "</div>";
          }
        } else {
          echo "<div class='summary-card'>";
          echo "<p>
          No results found.
          </p>
          </div>";
        } ?>
      </div>
    </div>
  </div>
