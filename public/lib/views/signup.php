<section class="wrap">
  <div class="container">
    <h1>Sign up</h1>
    <div>
      <form action='/signup' method='POST'>
        <input type='hidden' name='_method' value='post' />
        <div class="form-control">
          <label for='name' class="form__label">Username</label>
          <input type='text' id='name' name='name' autofocus required placeholder="Batman" class="form__input"/>
        </div>
        <div class="form-control">
          <label class="form__label">User type</label>
          <label for='user-type' class="form__label"><input type='radio' id='user-type' name='user-type'  required class="form__input-radio" value="normal"/> Normal</label>
          <label for='user-type1' class="form__label"><input type='radio' id='user-type1' name='user-type'  required class="form__input-radio" value="admin"/> Admin</label>
        </div>
        <div class="form-control">
          <label for='password' class="form__label">Password</label>
          <input type='password' id='password' name='password' required placeholder="@This1sAStrongP8SSW0RD@" class="form__input"/>
        </div>
        <div class="form-control">
          <label for='password-confirm' class="form__label">Confirm password</label>
          <input type='password' class="form__input" id='password-confirm' name='password-confirm' required placeholder="@This1sAStrongP8SSW0RD@"/>
        </div>
        <input type='submit' value='Sign up' class="btn"/>
      </form>
    </div>
  </div>
</section>
