<?php class User extends Database{

  public function is_authenticated(){
    $id = "";
    $hash="";

    session_start();
    if(!empty($_SESSION["id"]) && !empty($_SESSION["hash"])){
      $id = $_SESSION["id"];
      $hash = $_SESSION["hash"];
    }
    session_write_close();

    if(!empty($id) && !empty($hash)){

      try{
        $query = "SELECT hashed_password FROM users WHERE id=?";
        if($statement = $this->prepare($query)){
          $binding = array($id);
          if(!$statement -> execute($binding)){
            return false;
          }
          else{
            $result = $statement->fetch(PDO::FETCH_ASSOC);
            if($result['hashed_password'] === $hash){
              return true;
            }
          }
        }

      }
      catch(Exception $e){
        throw new Exception("Authentication not working properly. {$e->getMessage()}");
      }

    }
    return false;

  }


  public function is_admin(){
    $userType=false;
    $id = "";

    session_start();
    if(!empty($_SESSION["id"])){
      $id = $_SESSION["id"];
    }
    session_write_close();

    try{
      $query = "SELECT userType FROM users WHERE id=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          return false;
        }
        else{
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          if($result['userType'] === "admin"){
            return true;
          }
        }
      }

    }
    catch(Exception $e){
      return false;
    }
    return false;
  }

  public function getUserDetails(){
    $id = $this->get_user_id();
    try{
      $query = "SELECT name, userType FROM users where id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $results = $statement->fetch(PDO::FETCH_ASSOC);
          return $results;
        }
      }
      else{
        throw new Exception("Could not prepare statement.");

      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function updateUserName($username){
    $id = $this->get_user_id();
    try{
      $query = "UPDATE users SET name = ? where id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($username,$id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
      }
      else{
        throw new Exception("Could not prepare statement.");
      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function is_db_empty(){
    $is_empty = false;
    try{
      $query = "SELECT id FROM users WHERE id=?";
      if($statement = $this->prepare($query)){
        $id=1;
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          if(empty($result)){
            $is_empty = true;
          }
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return $is_empty;

  }

  public function get_users(){
    try{
      $query = "SELECT id, name FROM users";
      if($statement = $this->prepare($query)){
        $binding = array();
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $results = $statement->fetchall(PDO::FETCH_ASSOC);
          return $results;
        }
      }
      else{
        throw new Exception("Could not prepare statement.");

      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }


  public function sign_up($user_name, $userType, $password, $password_confirm){
    try{
      if($this->user_exists($user_name) == false){
        if($this->validate_user_name($user_name) && $this->validate_passwords($password,$password_confirm)){
          $salt = $this->generate_salt();
          $password_hash = $this->generate_password_hash($password,$salt);
          $query = "INSERT INTO users (name,userType,salt,hashed_password) VALUES (?,?,?,?)";
          if($statement = $this->prepare($query)){
            $binding = array($user_name,$userType,$salt,$password_hash);
            if(!$statement -> execute($binding)){
              throw new Exception("Could not execute query.");
            }
          }
          else{
            throw new Exception("Could not prepare statement.");

          }
        }
        else{
          throw new Exception("Please provide valid information.");
        }
      }
      else {
        throw new Exception("Username already exists.");
      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }

  }

  public function user_exists($username){
    try {
      $usersList = $this->get_users();
      for ($i=0; $i < count($usersList); $i++) {
        $users = $usersList[$i];
        foreach ($users as $key => $value) {
          if($key == "name" && $value == $username){
            return true;
          }
        }
      }
      return false;
    } catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return false;
  }

  public function get_user_id(){
    $id="";
    session_start();
    if(!empty($_SESSION["id"])){
      $id = $_SESSION["id"];
    }
    session_write_close();
    return $id;
  }

  public function get_user_name($id){
    $name=false;

    if(empty($id)){
      throw new Exception("User has no valid id");
    }

    try{
      $query = "SELECT name FROM users WHERE id=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          $name = $result['name'];
        }
      }
      else{
        throw new Exception("Could not prepare statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return $name;
  }

  public function get_user_type($id){
    $name="";

    if(empty($id)){
      throw new Exception("User has no valid id");
    }

    try{
      $query = "SELECT userType FROM users WHERE id=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          $name = $result['userType'];
        }
      }
      else{
        throw new Exception("Could not prepare statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return $name;
  }



  public function delete_user($id){
    if(empty($id)){
      throw new Exception("User has no valid id");
    }

    try {
      $userType = $this->get_user_type($id);
      if($userType != "" && $userType != "admin"){
        $query = "DELETE FROM users WHERE id=?";
        if($statement = $this->prepare($query)){
          $binding = array($id);
          if(!$statement -> execute($binding)){
            throw new Exception("Could not execute query.");
          }
        }
      }else {
        throw new Exception("Admins cannot be deleted!");
      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function sign_in($user_name,$password){
    try{
      $query = "SELECT id, salt, hashed_password FROM users WHERE name=?";
      if($statement = $this->prepare($query)){
        $binding = array($user_name);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else{
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          $salt = $result['salt'];
          $hashed_password = $result['hashed_password'];
          if($this->generate_password_hash($password,$salt) !== $hashed_password){
            throw new Exception("password or username is not valid");
          }
          else{
            $id = $result["id"];
            $this->set_authenticated_session($id,$hashed_password);
          }
        }
      }
      else{
        throw new Exception("Could not prepare statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }


  private function set_authenticated_session($id,$password_hash){
    session_start();

    //Make it a bit harder to session hijack
    session_regenerate_id(true);

    $_SESSION["id"] = $id;
    $_SESSION["hash"] = $password_hash;
    session_write_close();
  }

  private function generate_password_hash($password,$salt){
    return hash("sha256", $password.$salt, false);
  }

  private function generate_salt(){
    $chars = "0123456789ABCDEF";
    return str_shuffle($chars);
  }

  public function validate_user_name($user_name){
    if(!empty($user_name && strlen($user_name) > 3)){
      return true;
    }
    throw new Exception("username is not valid");
    return false;
  }

  public function validate_passwords($password, $password_confirm){
    if($password === $password_confirm && $this->validate_password($password)){
      return true;
    }
    throw new Exception("Password not valid. Please use atleast 8 charcters having number and letters");
    return false;
  }

  public function validate_password($password){
    if(!empty(password) && strlen($password) > 7){
      return true;
    }
    return false;
  }



  public function sign_out(){
    session_start();
    if(!empty($_SESSION["id"]) && !empty($_SESSION["hash"])){
      $_SESSION["id"] = "";
      $_SESSION["hash"] = "";
      $_SESSION = array();
      session_destroy();
    }
    session_write_close();
  }
}
