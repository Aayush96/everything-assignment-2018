<?php class Uploads extends Database{

  public function getAllUploads(){
    try {
      $query = "SELECT * FROM uploads";
      if($statement = $this->prepare($query)){
        if(!$statement->execute()){
          throw new Exception("Could not execute query.");
        } else {
          $results = $statement->fetchall(PDO::FETCH_ASSOC);
          return $results;
        }
      }
    }
    catch(Exception $e) {
      throw new Exception("Authentication not working properly. {$e->getMessage()}");
    }
    return null;
  }

  public function updateUploads($fileName, $year, $sem, $id){
    try {
      $query = "UPDATE uploads SET uploadsName = ?, year = ?, sem = ? WHERE id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($fileName, $year, $sem ,$id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function updateUploadsByUser($fileName, $year, $sem, $id){
    $userId=$this->get_user_id();

    try {
      $query = "UPDATE uploads SET uploadsName = ?, year = ?, sem = ? WHERE userId=? AND id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($fileName, $year, $sem, $userId ,$id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function getUploadsByCurrentUser(){
    $id=$this->get_user_id();

    try {
      $query = "SELECT * FROM uploads WHERE userId=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function getUploadsById($id){
    try {
      $query = "SELECT uploadsName,year,sem FROM uploads WHERE id=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          return $result;
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function getUploadsByIdForUser($id){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT uploadsName,year,sem FROM uploads WHERE id=? AND userId=?";
      if($statement = $this->prepare($query)){
        $binding = array($id, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          return $result;
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function deleteUpload($id){
    try {
      $query = "DElETE FROM uploads WHERE id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
      }
      else {
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function deleteUploadByUserUsingId($id){
    $userId=$this->get_user_id();

    try {
      $query = "DElETE FROM uploads WHERE userId=? AND id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($userId,$id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }

  public function getUploadPathById($id){
    try {
      $query = "SELECT filePath FROM uploads WHERE id = ?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetch(PDO::FETCH_ASSOC);
          return $result;
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function getYear(){
    $id=$this->get_user_id();

    try {
      $query = "SELECT year FROM uploads";
      if($statement = $this->prepare($query)){
        if(!$statement -> execute()){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;
        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function getYearByUser(){
    $id="";

    session_start();
    if(!empty($_SESSION["id"])){
      $id = $_SESSION["id"];
    }
    session_write_close();

    try {
      $query = "SELECT year FROM uploads WHERE userId=?";
      if($statement = $this->prepare($query)){
        $binding = array($id);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
      else{
        throw new Exception("Problem with statement.");
      }

    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
    return "";
  }

  public function searchYearSem($search, $year, $sem){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? AND year LIKE ? AND sem like ?";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $year = "%{$year}%";
        $sem = "%{$sem}%";
        $binding = array($search, $year, $sem);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchYear($search, $year){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? AND year LIKE ?";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $year = "%{$year}%";
        $binding = array($search, $year);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchSem($search, $sem){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE (LOWER(uploadsName) LIKE ? AND sem LIKE ?)";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $sem = "%{$sem}%";
        $binding = array($search, $sem);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;
        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function semYear($sem, $year){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE (sem LIKE ? AND year LIKE ?)";
      if($statement = $this->prepare($query)){
        $sem = "%{$sem}%";
        $year = "%{$year}%";
        $binding = array($sem, $year);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function search($search){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? ";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $binding = array($search);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;
        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function year($year){
    try {
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE year LIKE ? ";
      if($statement = $this->prepare($query)){
        $year = "%{$year}%";
        $binding = array($year);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function sem($sem){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE lower(sem) LIKE ? ";
      if($statement = $this->prepare($query)){
        $sem = "%{$sem}%";
        $binding = array($sem);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchYearSemByUser($search, $year, $sem){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? AND year LIKE ? AND sem like ? AND userId = ?";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $year = "%{$year}%";
        $sem = "%{$sem}%";
        $binding = array($search, $year, $sem, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchYearByUser($search, $year){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? AND year LIKE ? AND userId = ?";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $year = "%{$year}%";
        $binding = array($search, $year, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchSemByUser($search, $sem){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE (LOWER(uploadsName) LIKE ? AND sem LIKE ? AND userId = ?)";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $sem = "%{$sem}%";
        $binding = array($search, $sem, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function semYearByUser($sem, $year){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE (sem LIKE ? AND year LIKE ? AND userId = ?)";
      if($statement = $this->prepare($query)){
        $sem = "%{$sem}%";
        $year = "%{$year}%";
        $binding = array($sem, $year, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function searchByUser($search){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE LOWER(uploadsName) LIKE ? AND userId = ?";
      if($statement = $this->prepare($query)){
        $search = "%{$search}%";
        $binding = array($search, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function yearByUser($year){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE year LIKE ? AND userId = ?";
      if($statement = $this->prepare($query)){
        $year = "%{$year}%";
        $binding = array($year, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function semByUser($sem){
    try {
      $userId = $this->get_user_id();
      $query = "SELECT id, uploadsName, filePath, year, sem FROM uploads WHERE lower(sem) LIKE ? AND userId = ?";
      if($statement = $this->prepare($query)){
        $sem = "%{$sem}%";
        $binding = array($sem, $userId);
        if(!$statement -> execute($binding)){
          throw new Exception("Could not execute query.");
        }
        else {
          $result = $statement->fetchall(PDO::FETCH_ASSOC);
          return $result;

        }
      }
    } catch(Exception $e){
      throw new Exception("couldnot complete the search: {$e->getMessage()}");
    }
  }

  public function addItem($uploadsName,$filepath,$year,$sem){
    try{
      $id = $this->get_user_id();
      if($this->validateNotEmpty($year) && $this->validateNotEmpty($sem)){
        $query = "INSERT INTO uploads (userId,uploadsName,filepath,year,sem) VALUES (?,?,?,?,?)";
        if($statement = $this->prepare($query)){
          $binding = array($id, $uploadsName,$filepath,$year,$sem);
          if(!$statement -> execute($binding)){
            throw new Exception("Could not execute query.");
          }
        }
        else{
          throw new Exception("Could not prepare statement.");
        }
      }
      else{
        throw new Exception("Please provide valid information.");
      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }

  }

  public function get_user_id(){
    $id="";
    session_start();
    if(!empty($_SESSION["id"])){
      $id = $_SESSION["id"];
    }
    session_write_close();
    return $id;
  }

  public function validateNotEmpty($var){
    if(!empty($var)){
      return true;
    }
    return false;
  }

  public function delete_user($id){
    if(empty($id)){
      throw new Exception("User has no valid id");
    }

    try {
      $userType = $this->get_user_type($id);
      if($userType != "" && $userType != "admin"){
        $query = "DELETE FROM users WHERE id=?";
        if($statement = $this->prepare($query)){
          $binding = array($id);
          if(!$statement -> execute($binding)){
            throw new Exception("Could not execute query.");
          }
        }
      }else {
        throw new Exception("Admins cannot be deleted!");
      }
    }
    catch(Exception $e){
      throw new Exception($e->getMessage());
    }
  }
}
