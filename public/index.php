<?php
/* SET to display all warnings in development. Comment next two lines out for production mode*/
ini_set('display_errors','On');
error_reporting(E_ALL);

/* Set the path to the framework folder */
DEFINE("LIB",$_SERVER['DOCUMENT_ROOT']."/lib/");
DEFINE("CONF", $_SERVER['DOCUMENT_ROOT']."/config.php");

/* SET VIEW paths */
DEFINE("VIEWS",LIB."views/");
DEFINE("PARTIALS",VIEWS."partials/");
require CONF;
/* set the path to the Model classes folder */
DEFINE("MODELS",LIB."models");

/* Path to the Mouse application i.e. the Mouse Framework */
DEFINE("APP",LIB."app.php");

/* Define a default layout */
DEFINE("LAYOUT","standard");

//Define paths to static files
define("CSSPATH", "/build/main.bundle.css");
define("JSPATH", "/build/main.bundle.js");


require APP;


get("/",function($app){
  $app->force_to_https("/");
  $app->set_message("title","Home");
  try{
    $user = new User();
    if($user->is_authenticated()){
      $app->set_message("authenticated",true);
    }
  }catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
  $app->set_message("message","Welcome");
  $app->render(LAYOUT,"home");
});

get("/dashboard/", function($app){
  $app->force_to_https("/dashboard");
  try{
    $user = new User();
    $uploads = new Uploads();
    if($user->is_authenticated()){
      $app->set_message("authenticated",true);
      $id = $_SESSION['id'];
      $name = $user->get_user_name($id);
      $app->set_message("name",$name);

      if($user->is_admin()){
        $date = $uploads->getYear();
        $result = $uploads->getAllUploads();
      } else {
        $date = $uploads->getYearByUser();
        $result = $uploads->getUploadsByCurrentUser();
      }

      if(!empty($result)){
        $app->set_message("uploads",$result);
      } else{
        $app->set_message("uploads", null);
      }

      if(!empty($date)){
        $app->set_message("date",$date);
      } else{
        $app->set_message("date", null);
      }

    }
    else if($user->is_db_empty()){
      $app->redirect_to('/signup');
    }
    else {
      $app->set_flash("You are not authorised for this page. Try signing in.");
      $app->redirect_to('/');
    }

    $app->set_message("title","dashboard");
    $app->render(LAYOUT,"dashboard");
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
});

get("/dashboard/filter/:searchString", function($app){
  try{
    $user = new User();
    $uploads = new Uploads();
    if($user->is_authenticated()){
      $app->set_message("authenticated",true);
      $id = $_SESSION['id'];

      $searchVar = $_REQUEST['search'];
      $yearVar = $_REQUEST['year'];
      $semVar = $_REQUEST['sem'];
      $search = htmlspecialchars($searchVar);
      $year = htmlspecialchars($yearVar);
      $sem = htmlspecialchars($semVar);
      $app->set_message("search", $search);
      $app->set_message("year", $year);
      $app->set_message("sem", $sem);

      if($user->is_admin()){
        $date = $uploads->getYear();

        if($search != "" && $year != "" && $sem != "" ){
          $result = $uploads->searchYearSem($search,$year, $sem);
        }
        elseif($search != "" && $year != ""){
          $result = $uploads->searchYear($search,$year);
        }
        elseif($sem != "" && $year != ""){
          $result = $uploads->semYear($sem, $year);
        }
        elseif($search != "" && $sem != ""){
          $result = $uploads->searchSem($search,$sem);
        }
        elseif($search != ""){
          $result = $uploads->search($search);
        }
        elseif($year != ""){
          $result = $uploads->year($year);
        }
        elseif($sem != ""){
          $result = $uploads->sem($sem);
        }

      } else {
        $date = $uploads->getYearByUser();

        if($search != "" && $year != "" && $sem != "" ){
          $result = $uploads->searchYearSemByUser($search,$year, $sem);
        }
        elseif($search != "" && $year != ""){
          $result = $uploads->searchYearByUser($search,$year);
        }
        elseif($sem != "" && $year != ""){
          $result = $uploads->semYearByUser($sem, $year);
        }
        elseif($search != "" && $sem != ""){
          $result = $uploads->searchSemByUser($search,$sem);
        }
        elseif($search != ""){
          $result = $uploads->searchByUser($search);
        }
        elseif($year != ""){
          $result = $uploads->yearByUser($year);
        }
        elseif($sem != ""){
          $result = $uploads->semByUser($sem);
        }
      }

      if(!empty($result)){
        $app->set_message("uploads",$result);
      } else{
        $app->set_message("uploads", null);
      }

      if(!empty($date)){
        $app->set_message("date",$date);
      } else{
        $app->set_message("date", null);
      }

    }
    else if($user->is_db_empty()){
      $app->redirect_to('/signup');
    }
    else {
      $app->set_flash("You are not authorised for this page. Try signing in.");
      $app->redirect_to('/');
    }

    $app->set_message("title","dashboard");
    $app->render(LAYOUT,"results");
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
});

get("/members",function($app){
  $app->force_to_https("/members");
  try{
    $user = new User();
    if($user->is_admin()){
      if($user->is_authenticated()){
        $app->set_message("authenticated",true);
        $results = $user->get_users();
        $app->set_message("users",$results);
      }
      else if($user->is_db_empty()){
        $app->redirect_to('/signup');
      }
      else {
        $app->set_flash("You are not authorised for this page. Try signing in.");
        $app->redirect_to('/');
      }

      $app->set_message("title","Members");
      $app->render(LAYOUT,"members");
    } else {
      $app->set_flash("Insufficient Access");
      $app->redirect_to("/");
    }
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
});

get("/member/:id",function($app){
  $id = $app->route_var('id');
  if(is_numeric($id)){

    $app->force_to_https("/member/{$id}");
    $name="";

    try{
      $user = new User();
      if($user->is_admin()){
        if($user->is_authenticated()){
          $app->set_message("authenticated",true);
          $name = $user->get_user_name($app->route_var("id"));
          if(empty($name)){
            $name = "NO SUCH USER";
          }
        }
        else if($user->is_db_empty()){
          $app->redirect_to('/signup');
        }
        else{
          $app->set_flash("You are not authorised to see Members pages. Sign in first.");
          $app->redirect_to('/signin');
        }
        $app->set_message("title","Members");
        $app->set_message("name",$name);
        $app->render(LAYOUT,"member");
      } else {
        $app->set_flash("Insufficient Access");
        $app->redirect_to("/");
      }
    }
    catch(Exception $e){
      $app->set_flash("error, ",$e->getMessage());
      $app->redirect_to('/home');
    }
  }
  else{
    $app->reset_route();
  }
});


get("/signup",function($app){
  $app->force_to_https("/signup");
  $is_authenticated=false;
  $is_db_empty=false;

  try {
    $user = new User();
    $is_authenticated = $user->is_authenticated();
    $is_db_empty = $user->is_db_empty();
  }
  catch(Exception $e){
    $app->set_flash("We have a problem with DB. The gerbils are working on it.");
    $app->redirect_to("/signup");
  }

  if($is_authenticated){
    $app->set_message("error","Create more accounts for other users.");
    $app->set_message("authenticated",$is_authenticated);
  }
  $app->set_message("title","Sign up");
  $app->render(LAYOUT,"signup");
});

get("/account/", function($app){
  $app->force_to_https("/account");
  try{
    $user = new User();
    if($user->is_authenticated()){
      $app->set_message("authenticated",true);
      $id = $_SESSION['id'];

      $result = $user->getUserDetails();

      if(!empty($result)){
        $app->set_message("result", $result);
      }else{
        $app->set_flash("No item for that {$itemId} present");
        $app->redirect_to("/account");
      }

    } else {
      $app->set_flash("Tou are not authorised for this page. Try signing in");
      $app->redirect_to("/");
    }
  } catch(Exception $e){
    $app->set_Flash($e->getMessage());
  }
  $app->set_message("title","Account");
  $app->render(LAYOUT,"account");
});

post("/account/update", function($app){
  $app->force_to_https("/account/update");
  try{
    $user = new User();
    $userName = $app->form('name');
    $users = $user->get_users();
    $id = $user->get_user_id();

    $userAlreadyPresent = true;
    for($i = 0; $i < count($users); $i++){
      $userAc = $users[$i];

      if ($userAc['name'] == $username) {
        $userAlreadyPresent = true;
      } else{
        $userAlreadyPresent = false;
      }
    }

    if($userAlreadyPresent == false){
      $user->updateUserName($userName);
      $app->set_flash("Username updated to {$username} updated successfully");
    } else{
      $app->set_flash("Username already taken");
    }

  } catch(Exception $e){
    $app->set_Flash($e->getMessage());
  }
  $app->redirect_to("/account");
});

get("/item/:itemId", function($app){
  $app->force_to_https("/item:itemid");
  try{
    $user = new User();
    $uploads = new Uploads();
    if($user->is_authenticated()){
      $app->set_message("authenticated",true);
      $id = $_SESSION['id'];
      $itemId = $app->route_var("itemId");
      $app->set_message("itemId", $itemId);

      if($user->is_admin()){
        $result = $uploads->getUploadsById($itemId);

      } else {
        $result = $uploads->getUploadsByIdForUser($itemId);
      }
      if(!empty($result)){
        $app->set_message("result", $result);
      }else{
        $app->set_flash("No item for that {$itemId} present");
        $app->redirect_to("/dashboard");
      }

    } else {
      $app->set_flash("Tou are not authorised for this page. Try signing in");
      $app->redirect_to("/");
    }
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
  $app->set_message("title","Sign up");
  $app->render(LAYOUT,"itemUpdate");
});

post("/itemUpdate", function($app){
  $app->force_to_https("/itemUpdate");
  try {
    $user = new User();
    $uploads = new Uploads();
      $fileName = $app->form('name');
      $year = $app->form('year');
      $sem = $app->form('sem');
      $id = $app->form('id');
      $app->set_session_message("itemId", $id);
      if(isset($fileName) && isset($year) && isset($sem) && isset($id)){

        if($user->is_admin()){
          $result = $uploads->updateUploads($fileName, $year,$sem,$id);
          $app->set_flash("Sucessfully Updated");
        } else {
          $result = $uploads->updateUploadsByUser($fileName,$year,$sem,$id);
          $app->set_flash("Sucessfully Updated");
        }

      } else {
        $app->set_flash("Please provide data for the fields");
        $id = $app->get_session_message("itemId");
        $app->redirect_to("/item/{$id}");
      }

  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
  $id = $app->get_session_message("itemId");
  $app->redirect_to("/item/{$id}");

});


get("/signout",function($app){
  // should this be GET or POST or PUT?????
  $app->force_to_https("/signout");

  try{
    $user = new User();

    if($user->is_authenticated()){
      $user->sign_out();
      $app->set_flash("You are now signed out.");
      $app->redirect_to("/");
    }
    else{
      $app->set_flash("You can't sign out if you are not signed in!");
      $app->redirect_to("/");
    }
  }
  catch(Exception $e){
    $app->set_flash("Something wrong with the sessions.");
    $app->redirect_to("/");
  }
});

post("/upload",function($app){
  try{
    $user = new User();
    if($user->is_authenticated()){
      $id = $_SESSION['id'];
      if($id != ""){
        $file = $_FILES["upload-file"];
        if(isset($file)){
          //set the file in the id of the user
          $target_dir = $_SERVER['DOCUMENT_ROOT']."/uploads/{$id}/";
          $target_file = $target_dir.basename($file["name"]);
          // echo "$target_file";

          if (is_dir($target_dir) == false) {
            mkdir($target_dir);
            $app->set_flash("{$target_dir} created");
          }
          // Check if file already exists
          $target_file_name =  $app->fileExists($target_file);
          $app->set_flash(" target  name: {$target_file_name}");

          //  move the file
          if (move_uploaded_file($file['tmp_name'], $target_file_name)) {
            try {
              $uploads = new Uploads();
              $year = htmlspecialchars($app->form("year"));
              $sem = htmlspecialchars($app->form("sem"));
              $uploads->addItem(basename($file["name"]),$target_file_name, $year, $sem);
              $app->set_flash("The file ". basename($file['name']). " has been uploaded.");
            } catch (\Exception $e) {
              unlink($target_file_name);
              $app->set_flash("Uploads failed");
            }
            $app->redirect_to("/dashboard");
          } else {
            $app->set_flash("There was an error uploading file.");
            $app->redirect_to("/dashboard");
          }
          //
        } else{
          $app->set_flash("Please, choose a file to upload");
          $app->redirect_to("/dashboard");
        }
      }
    }
    else{
      $app->set_flash("Please, Signin");
      $app->redirect_to("/dashboard");
    }
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
});

post("/signup",function($app){

  try{
    $user = new User();
    $name = $app->form('name');
    $pw = $app->form('password');
    $confirm = $app->form('password-confirm');
    $userType = $app->form('user-type');

    if($name && $pw && $confirm){
      try{
        $user->sign_up($name,$userType, $pw,$confirm);
        $app->set_flash(htmlspecialchars($app->form('name'))." is now signed up");
      }
      catch(Exception $e){
        $app->set_flash($e->getMessage());
        $app->redirect_to("/signup");
      }
    }
    else{
      $app->set_flash("You are not signed up. Try again and don't leave any fields blank.");
      $app->redirect_to("/signup");
    }
    $app->redirect_to("/signup");
  }
  catch(Exception $e){
    $app->set_flash($e.getMessage());
    $app->redirect_to("/");
  }
});

post("/item/delete/", function($app){
  $app->force_to_https("/item/delete");
  try {
    $id = $app->form('id');
    if($id != ""){
      $uploads = new Uploads();
      $user = new User();
      if($user->is_admin()){

        $uploadsPath = $uploads->getUploadPathById($id);
        $path = $uploadsPath['filePath'];
        if(file_exists($path)){
          // echo "deleted {$path}";
          unlink($path);
        }
        $uploads->deleteUpload($id);
        $app->set_flash("Item deleted");
      } else {
        try {
          $uploadsPath = $uploads->getUploadPathById($id);
          $path = $uploadsPath['filePath'];
          // echo "deleted {$path}";

          if(file_exists($path)){
            // echo "deleted {$path}";
            unlink($path);
          }
          $uploads->deleteUploadByUserUsingId($id);
          // $app->set_flash("Item deleted");
        } catch (Exception $e) {
          $app->set_flash("Couldnot delete the data");
        }
      }
    } else {
      $app->set_flash("Please, provide item Id to delete");
    }
  }
  catch(Exception $e){
    $app->set_message("error, ",$e->getMessage());
  }
  $app->redirect_to("/dashboard");
});



post("/",function($app){
  $name = $app->form('username');
  $password = $app->form('password');
  if($name && $password){

    try {
      $user = new User();
      $user->sign_in($name,$password);
    }
    catch(Exception $e){
      $app->set_flash("Could not sign you in. Try again. {$e->getMessage()}");
      $app->redirect_to("/");
    }
  }
  else {
    $app->set_flash("Something wrong with name or password. Try again.");
    $app->redirect_to("/");
  }
  $app->set_flash("Lovely, you are now signed in!");
  if($user->is_admin() == false){
    $app->redirect_to("/dashboard");
  }
  $app->redirect_to("/members");
});


delete("/members/:id",function($app){
  $id = $app->route_var('id');
  if(is_numeric($id)){
    $name = "UNKNOWN";

    try{
      $user = new User();
      $name = $user->get_user_name($id);
      $user->delete_user($id);
    }
    catch(Exception $e){
      $app->set_flash("Could not delete record. {$e->getMessage()}");
      $app->redirect_to("/members");

    }
    $app->set_flash("{$name} was deleted.");
    $app->redirect_to("/members");
  }
  else{
    $app->reset_route();
  }
});


resolve();
