
module.exports = class CamFeed {

  constructor(options) {
    let defaults = {
      videoOutputClass: '.cam-video',
      errorClass: '.class__cam-error',
      outputImgClass: '.capturedImage',
      takeAttendanceBtn: '.takeAttendance'
    };

    let populated = Object.assign(defaults, options);
    for (let key in populated) {
      if (populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }

    this.getCamFeed(this);
  }

  getCamFeed(main) {
    var video = document.querySelector(main.videoOutputClass);
    var img = document.querySelector(main.outputImgClass);
    var btn = document.querySelector(main.takeAttendanceBtn)

    // var

    if (video) {
      var constraints = {
        audio: false,
        video: {
          width: 250,
          height: 200
        }
      };

      navigator.mediaDevices.getUserMedia(constraints)
      .then((mediaStream) => {

        video.srcObject = mediaStream;
        console.log(mediaStream);
        if(btn && img){
          btn.addEventListener("click", () => {
            var imageCap = new ImageCapture(mediaStream.getVideoTracks()[0]);

            imageCap.takePhoto().then((photoData) => {

              img.src = URL.createObjectURL(photoData);
              console.log(img.src);
              console.log(img);
              let formData = new FormData();
              let reader = new FileReader();

              var image = createImageBitmap(photoData);

              console.log(image);

              reader.onload = function (file) {
                let rawData = file.target.result;
                // console.log(e);
                // formData.append('fileName', data);
                console.log(rawData);
                formData.append('file', rawData);
                fetch("./lib/fileUpload.php", {

                  method: 'POST',
                  body: formData

                }).then((response) => {
                  console.log(Response);
                  // return response.json();
                }).catch((err) => {
                  console.log(err);
                });
              }

              reader.readAsDataURL(photoData);

            })
            .catch((err) => {
              console.log(err);
            });

            // return imageCap.takePhoto();
          });

          video.onloadedmetadata = function(e) {
            video.play();
          };

        }
      })
      .catch(function(err) {
        console.log(err.name + ": " + err.message);
        var errorOutput = document.querySelector(main.errorClass);
        errorOutput.textContent = err.name + ": " + err.message;
      });
    }
  }

};
