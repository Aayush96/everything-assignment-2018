
module.exports = class ajaxTest{

  constructor(options){
    let defaults = {
    };

    let populated = Object.assign(defaults, options);
    for(let key in populated) {
      if(populated.hasOwnProperty(key)) {
        this[key] = populated[key];
      }
    }
    this.ajaxCall(this);
  }

  ajaxCall(that){
    var btn = document.querySelector(".ajaxTestBtn");

    btn.addEventListener('click', that.loadText);
  }

  //HTTP statuses
  //200: "OK"
  //403: "forbidden"
  //404: "not found"
  loadText(){
    var xhr = new XMLHttpRequest();
    console.log(xhr);
    //create a request
    xhr.open('GET', 'https://api.github.com/users', true);

    xhr.onload = function (){
      if(this.status == 200){
        var users = JSON.parse(this.responseText);
        console.log(users);
        var i = 1;
        users.map((user)=>{
          console.log(`User ${i}: ${user.login}`);
          i++;
        });
      } else {
        console.log("request failed");
      }
    };

    xhr.onerror  = function (data) {
      console.log("request error" + data);
    };

    //send the request
    xhr.send();

  }

};
